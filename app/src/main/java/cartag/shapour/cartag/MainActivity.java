package cartag.shapour.cartag;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter btAdapter;
    private final int ENABLE_BT_REQUEST = 1;

    private ArrayList<BluetoothDevice> nbDevices = null;
    private ListView devicesView;
    private ArrayAdapter<BluetoothDevice> devicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            Log.e("CarTag","BT not available");
        } else {
            if (btAdapter.isEnabled()) {
                Log.e("CarTag","BT is available");
                devicesView = (ListView) findViewById(R.id.available_devices);
                nbDevices = new ArrayList<>();
                for (BluetoothDevice device : btAdapter.getBondedDevices()) {
                    nbDevices.add(device);
                }
                devicesAdapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, nbDevices);
                devicesView.setAdapter(devicesAdapter);
            }
            else {
                Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(i, ENABLE_BT_REQUEST);
            }
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_BT_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.e("CarTag", "BT enabled by user");
        } else if (requestCode == ENABLE_BT_REQUEST && resultCode == Activity.RESULT_CANCELED) {
            Log.e("CarTag","BT enabling canceled");
        }
    }
}
